#pragma once

#include <await/executors/executor.hpp>

namespace whirl {

// Execution

using await::executors::IExecutor;
using await::executors::IExecutorPtr;
using await::executors::Task;

}  // namespace whirl
