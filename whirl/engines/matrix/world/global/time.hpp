#pragma once

#include <whirl/time.hpp>

namespace whirl::matrix {

TimePoint GlobalNow();

}  // namespace whirl::matrix
