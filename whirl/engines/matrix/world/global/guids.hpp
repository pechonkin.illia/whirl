#pragma once

#include <whirl/services/guid.hpp>

namespace whirl::matrix {

Guid GenerateGuid();

}  // namespace whirl::matrix
