#pragma once

#include <string>

namespace whirl::rpc {

// Decoded input/output data
using BytesValue = std::string;

}  // namespace whirl::rpc
